<?php

/*
Plugin Name: Add Classes To Tags
Plugin URI: https://planetjon.ca/projects/add-classes-to-tags/
Description: Decorate tag links with classes
Version: 1.0
Requires at least: 3.5.0
Tested up to: 5.0
Requires PHP: 5.4
Author: Jonathan Weatherhead
Author URI: https://planetjon.ca
License: GPL2
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

namespace planetjon\add_classes_to_tags;

// annotate tag links with classes
add_filter( 'term_links-post_tag', function( array $links ) {
    return preg_replace_callback(
        '|href="[^"]*/([^"]+?)/?"|',
        function( array $matches ) {
            list( $href, $slug ) = $matches;
            return "class=\"tag-{$slug}\" {$href}";
        },
        $links
    );
});